#  CTK Test App

This is a test app to illustrate how building a project using xcodebuild fails when using a persistent token from a CryptoTokenKit extension. The Signing App is a container app with a CryptoTokenKit extension that has a identity embedded inside it. When the app is run, a certificate is added to the system that can be used for code signing. When the cn of the certificate is specificed with codesign command line tool, the signing works fine:

`cp -R /System/Applications/TextEdit.app /tmp`
`codesign -fs "Twocanoes Test Code Signing 2" /tmp/TextEdit.app`

When using the same identity in Xcode, it shows up in the list of certificates in Build Settings->Code Signing Identites, but fails right at the start of the build:


    Showing All Messages
    No certificate for team 'Twocanoes Software, Inc.' matching 'Twocanoes Test Code Signing 2' found: Select a different signing certificate for CODE_SIGN_IDENTITY, a team that matches your selected certificate, or switch to automatic provisioning.


The same error occurs when using xcodebuild on the command line:
    mdscentral-6:SignMe tperfitt$ xcodebuild CODE_SIGN_IDENTITY="Twocanoes Test Code Signing 2" CODE_SIGN_STYLE="Manual"
    Command line invocation:
        /Applications/Xcode.app/Contents/Developer/usr/bin/xcodebuild "CODE_SIGN_IDENTITY=Twocanoes Test Code Signing 2" CODE_SIGN_STYLE=Manual

    Build settings from command line:
        CODE_SIGN_IDENTITY = Twocanoes Test Code Signing 2
        CODE_SIGN_STYLE = Manual

    note: Using new build system
    note: Using codesigning identity override: Twocanoes Test Code Signing 2
    note: Planning build
    note: Using build description from disk
    error: No certificate for team 'UXP6YEHSPW' matching 'Twocanoes Test Code Signing 2' found: Select a different signing certificate for CODE_SIGN_IDENTITY, a team that matches your selected certificate, or switch to automatic provisioning. (in target 'SignMe' from project 'SignMe')

To use the app:

1. Download the zip app for the [downloads](https://bitbucket.org/twocanoes/signingapp/downloads/) and unzip
2. Download the root ca certicate ("ca.twocanoes.com.cer") from [downloads](https://bitbucket.org/twocanoes/signingapp/downloads/) and add to the login keychain. It does not need to be marked as trusted.
2. Run the app
3. Copy the codesign command and run in terminal. Verify it is correct.
4. In Xcode, create a new sample project
5. In terminal, cd to the sample project and run :
`xcodebuild CODE_SIGN_IDENTITY="Twocanoes Test Code Signing 2" CODE_SIGN_STYLE="Manual"`

