//
//  TokenSession.swift
//  SigningExtension
//
//  Created by Timothy Perfitt on 3/3/21.
//

import CryptoTokenKit

class TokenSession: TKTokenSession, TKTokenSessionDelegate {

    func tokenSession(_ session: TKTokenSession, beginAuthFor operation: TKTokenOperation, constraint: Any) throws -> TKTokenAuthOperation {
        // Insert code here to create an instance of TKTokenAuthOperation based on the specified operation and constraint.
        // Note that the constraint was previously established when creating token configuration with keychain items.
        return TKTokenPasswordAuthOperation()
    }
    
    func tokenSession(_ session: TKTokenSession, supports operation: TKTokenOperation, keyObjectID: Any, algorithm: TKTokenKeyAlgorithm) -> Bool {
            // Indicate whether the given key supports the specified operation and algorithm.

            switch operation {
            case .signData:
                return (algorithm.isAlgorithm(SecKeyAlgorithm.rsaSignatureRaw)) && (algorithm.supportsAlgorithm(SecKeyAlgorithm.rsaSignatureDigestPKCS1v15Raw))

            default:
                break
            }
            return false
        }


    
    func tokenSession(_ session: TKTokenSession, sign dataToSign: Data, keyObjectID: Any, algorithm: TKTokenKeyAlgorithm) throws -> Data {
        var signature: Data?
        
        guard let pathURL = Bundle.main.url(forResource: "codesign", withExtension: "pem") else {

            throw NSError(domain: TKErrorDomain, code: TKError.Code.communicationError.rawValue, userInfo: nil)
        }
        if let data = try? Data.init(contentsOf: pathURL, options: .uncached) {
            let (key,_) = IdentityUtil.shared.importPEM(pemData: data)

            if let key = key {
                signature = IdentityUtil.shared.signData(data: dataToSign, key: key)
            }
        }

        if let signature = signature {
            return signature
        } else {
            // If the operation failed for some reason, fill in an appropriate error like objectNotFound, corruptedData, etc.
            // Note that responding with TKErrorCodeAuthenticationNeeded will trigger user authentication after which the current operation will be re-attempted.
            throw NSError(domain: TKErrorDomain, code: TKError.Code.authenticationNeeded.rawValue, userInfo: nil)
        }
    }
    
    func tokenSession(_ session: TKTokenSession, decrypt ciphertext: Data, keyObjectID: Any, algorithm: TKTokenKeyAlgorithm) throws -> Data {
        var plaintext: Data?
        
        // Insert code here to decrypt the ciphertext using the specified key and algorithm.
        plaintext = nil
        
        if let plaintext = plaintext {
            return plaintext
        } else {
            // If the operation failed for some reason, fill in an appropriate error like objectNotFound, corruptedData, etc.
            // Note that responding with TKErrorCodeAuthenticationNeeded will trigger user authentication after which the current operation will be re-attempted.
            throw NSError(domain: TKErrorDomain, code: TKError.Code.authenticationNeeded.rawValue, userInfo: nil)
        }
    }
    
    func tokenSession(_ session: TKTokenSession, performKeyExchange otherPartyPublicKeyData: Data, keyObjectID objectID: Any, algorithm: TKTokenKeyAlgorithm, parameters: TKTokenKeyExchangeParameters) throws -> Data {
        var secret: Data?
        
        // Insert code here to perform Diffie-Hellman style key exchange.
        secret = nil
        
        if let secret = secret {
            return secret
        } else {
            // If the operation failed for some reason, fill in an appropriate error like objectNotFound, corruptedData, etc.
            // Note that responding with TKErrorCodeAuthenticationNeeded will trigger user authentication after which the current operation will be re-attempted.
            throw NSError(domain: TKErrorDomain, code: TKError.Code.authenticationNeeded.rawValue, userInfo: nil)
        }
    }
}
