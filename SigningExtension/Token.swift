//
//  Token.swift
//  SigningExtension
//
//  Created by Timothy Perfitt on 3/3/21.
//

import CryptoTokenKit

class Token: TKToken, TKTokenDelegate {

    func createSession(_ token: TKToken) throws -> TKTokenSession {
        return TokenSession(token:self)
    }

}
