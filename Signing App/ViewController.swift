//
//  ViewController.swift
//  Signing App
//
//  Created by Timothy Perfitt on 3/3/21.
//

import Cocoa

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let pathURL = Bundle.main.url(forResource: "codesign", withExtension: "pem") else {
            return
        }
        if let data = try? Data.init(contentsOf: pathURL, options: .uncached) {
            let (_, cert) = IdentityUtil.shared.importPEM(pemData: data)
            if let cert = cert{
                let certData = SecCertificateCopyData(cert)
                TokenManager.insertConfig(configName: "test", certificates:[certData as Data] )


            }

        }
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

