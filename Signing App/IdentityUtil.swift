//
//  IdentityUtil.swift
//  Signing App
//
//  Created by Timothy Perfitt on 3/3/21.
//

import Foundation

class IdentityUtil: NSObject {

    static var shared = IdentityUtil()

//    init() {
//
//    }

    func importPEM(pemData: Data) -> (SecKey?, SecCertificate?){


        var inputFormat :SecExternalFormat = .formatBSAFE
        var itemType :SecExternalItemType = .itemTypeUnknown
        var itemsCFArray :CFArray? = nil

        let error = SecItemImport(pemData as CFData, "import.pem" as CFString, &inputFormat, &itemType, [], nil, nil, &itemsCFArray)


        if error != 0 {
            print(error)
            return (nil,nil)
        }
        guard let array:NSArray = itemsCFArray else {
            return (nil,nil)
        }
        let key = array[0] as! SecKey
        let cert = array[1] as! SecCertificate

        return (key,cert)
    }

    func signData(data:Data,key:SecKey) -> Data? {
        if let signature = SecKeyCreateSignature(key, .rsaSignatureRaw, data as CFData, nil){

            return signature as Data
        }
        return nil


    }
}


