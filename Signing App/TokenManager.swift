//
//  TokenManager.swift
//  SmartCard Utility
//
//  Created by Timothy Perfitt on 11/2/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

import CryptoTokenKit

@available(OSX 10.15, *)
@objc class TokenManager: NSObject {
    class func removeAllConfigs(configID:String) {
        let driverConfig = TKTokenDriver.Configuration.driverConfigurations

        if let configurationInfo = driverConfig[configID]{
            let tokenConfigs = configurationInfo.tokenConfigurations

            for (key,_) in tokenConfigs {
                configurationInfo.removeTokenConfiguration(for: key)
            }

        }

    }

    class func removeToken(configID:String, tokenIdentifer: String) {
        let driverConfig = TKTokenDriver.Configuration.driverConfigurations
        let configurationInfo = driverConfig[configID]
        configurationInfo?.removeTokenConfiguration(for: tokenIdentifer)

    }

    @objc class func insertConfig(configName:String, certificates:Array<Data> ) {

        var keyItems = [TKTokenKeychainItem]()
        let driverConfig = TKTokenDriver.Configuration.driverConfigurations

        if let tokenConfig = driverConfig["com.twocanoes.Signing-App.SigningExtension"] {
            let newConfig = tokenConfig.addTokenConfiguration(for: configName)

            let constraint = "NONE"

            let constraints : [ NSNumber : TKTokenOperationConstraint] = [
                TKTokenOperation.readData.rawValue as NSNumber : constraint as TKTokenOperationConstraint,
                TKTokenOperation.decryptData.rawValue as NSNumber : constraint as TKTokenOperationConstraint,
                TKTokenOperation.none.rawValue as NSNumber: constraint as TKTokenOperationConstraint,
                TKTokenOperation.performKeyExchange.rawValue as NSNumber: constraint as TKTokenOperationConstraint,
                TKTokenOperation.signData.rawValue as NSNumber: constraint as TKTokenOperationConstraint
            ]

            var i = 0
            for currCert in certificates {
                if let certificate = SecCertificateCreateWithData(nil, currCert as CFData){


                    let keychainCertificate = TKTokenKeychainCertificate.init(certificate: certificate, objectID:UUID.init().uuidString)
                    i=i+1
                    keyItems.append(keychainCertificate!)

                    if let certItemKey = TKTokenKeychainKey.init(certificate: certificate, objectID: currCert) {
                        certItemKey.canSign = true
                        certItemKey.canDecrypt = true
                        certItemKey.canPerformKeyExchange = true
                        certItemKey.isSuitableForLogin = true
                        certItemKey.constraints = constraints
                        keyItems.append(certItemKey)
                    }

                }

            }
            newConfig.keychainItems = keyItems


        }
    }
}
